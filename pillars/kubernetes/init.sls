kubernetes:
  minor_version: 28
  patch_version: 4
  pod-network-cidr: 10.244.0.0/16
  service-dns-domain: k8s.delauney.home
  master:
    hostname: kube-master-1.delauney.home
    ip: 192.168.14.10
  kubeadm-init:
    cmd-args: --pod-network-cidr 10.244.0.0/16 --service-dns-domain k8s.delauney.home
  join-cmd: 'kubeadm join 192.168.11.1:6443 --token 8h0m5p.5cy1q9jnmqv5x17p --discovery-token-ca-cert-hash sha256:54b17cdaedd2d5556c1a2725cc108d53cabc3729febf9bdf0b2ce24d494d8c6e'

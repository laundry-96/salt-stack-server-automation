laundry-account:
  password: "$6$nbwRFu/8e0upBJe8$7lB7hzdh8G/E/.5M/FEHhq9MpFYy4SNC7xoRWDGstSnycL6rUpNhDD9E2jlzxqbV6nBIc90d4xicU/x5tTSY5."
  ssh_keys: 
    framework: 
      key: AAAAC3NzaC1lZDI1NTE5AAAAINsSQfhlwbuzf51/m/A+Rk73F84cTlYy8Bq784Sqg5LS
      enc: ssh-ed25519
    #willow: 
    #  key: AAAAC3NzaC1lZDI1NTE5AAAAIBrE+48yFdunH6Ol1VU1oSBvqL4BigFvY0rhDUH3ZiPP
    #  enc: ssh-ed25519
    #weeping-willow:
    # key: AAAAC3NzaC1lZDI1NTE5AAAAIHb8OJQq8SQnlbZTaUVuevsOgL6ZZZKzRgdRl8S8uSq5
    # enc: ssh-ed25519

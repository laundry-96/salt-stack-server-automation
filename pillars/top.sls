base:
  '*':
    - laundry
    - domain-name
    - cashd

  'laundry-*':
    - awky

  'jumpbox':
    - jumpbox

  'lcarn-*':
    - lcarn

  'valkyrie-*':
    - valkyrie

  'awky-*':
    - awky
   
  'kube*':
    - kubernetes
  'etcd*':
    - kubernetes

  'minecraft-public':
    - minecraft

  'seedbox*':
    - nordvpn

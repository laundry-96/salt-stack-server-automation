jumpbox-users:
  awky:
    name: Ashley Wu
    jump-keys:
      work:
        jump-key: AAAAC3NzaC1lZDI1NTE5AAAAIOrP5+tgMevWkzl0M/6wvTD3+DV/jQgDE9s1vb9x3Ke1
  lcarn:
    name: Luke Carnaggio
    jump-keys:
      personal:
        jump-key: AAAAC3NzaC1lZDI1NTE5AAAAIKUHYZAHdVblX5uAF1vrnkJWrrVWd6jrYmkrwbzdDB2e
  valkyrie:
    name: Valarie Novak
    jump-keys:
      personal:
        jump-key: AAAAC3NzaC1lZDI1NTE5AAAAIJSgpPUs2FBOnaVEo/5tmhj1yL4t9w4FxY40do0FYQ4a

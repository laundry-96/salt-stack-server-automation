minecraft-server:
  forge:
    version: 1.15.2-31.2.0
    link: https://files.minecraftforge.net/maven/net/minecraftforge/forge/1.15.2-31.2.0/forge-1.15.2-31.2.0-installer.jar
    checksum: sha256=a740c2d4e9fccef65c51ca2c2d5041159c04efc461dc73bbca691e87bd319ba4
  properties:
    motd: Laundry's Modded Minecraft Server, powered by SaltStack!
    view-distance: 15
    online-mode: true
    port: 25564
    difficulty: hard


overviewer:
  output_dir: /opt/overviewer/renders
  textures:
    version: 1.12
    checksum: sha256=59e84cf6acc3912e1705ab05c722485afb3ec33343eef4756d8c16edfe6411ca

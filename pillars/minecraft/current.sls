include:
  - minecraft

minecraft:
  servers:
    vanilla:
      started: yes
      accept_eula: yes
      msm:
        jar-group: vanilla
        version: minecraft/1.17.1
        ram: 4096

minecraft-server:
  jar: 
    version: 1.16.4
    checksum: sha256=444d30d903a1ef489b6737bb9d021494faf23434ca8568fd72ce2e3d40b32506
    link: https://launcher.mojang.com/v1/objects/35139deedbd5182953cf1caa23835da59ca3d7cd/server.jar
  properties:
    motd: Laundry's Public Minecraft Server, powered by SaltStack!
    view-distance: 15
    online-mode: true
    port: 25564
    difficulty: normal
    level-seed: -8120374384168946562


overviewer:
  output_dir: /opt/overviewer/renders
  textures:
    version: 1.12
    checksum: sha256=59e84cf6acc3912e1705ab05c722485afb3ec33343eef4756d8c16edfe6411ca

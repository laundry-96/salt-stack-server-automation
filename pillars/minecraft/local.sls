minecraft-server:
  jar: 
    version: 1.16.1
    checksum: sha256=2782d547724bc3ffc0ef6e97b2790e75c1df89241f9d4645b58c706f5e6c935b
    link: https://launcher.mojang.com/v1/objects/a412fd69db1f81db3f511c1463fd304675244077/server.jar
  properties:
    motd: Laundry's Local Minecraft Server, powered by SaltStack!
    view-distance: 15
    online-mode: true
    port: 25565
    difficulty: normal


overviewer:
  output_dir: /opt/overviewer/renders
  textures:
    version: 1.12
    checksum: sha256=59e84cf6acc3912e1705ab05c722485afb3ec33343eef4756d8c16edfe6411ca

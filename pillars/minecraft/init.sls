minecraft-server:
  jar: 
    version: 1.17.1
    checksum: sha256=e8c211b41317a9f5a780c98a89592ecb72eb39a6e475d4ac9657e5bc9ffaf55f
    link: https://launcher.mojang.com/v1/objects/a16d67e5807f57fc4e550299cf20226194497dc2/server.jar
  properties:
    motd: Laundry's Minecraft Server, powered by SaltStack!
    view-distance: 20
    online-mode: true


overviewer:
  output_dir: /opt/overviewer/renders
  textures:
    version: 1.12
    checksum: sha256=59e84cf6acc3912e1705ab05c722485afb3ec33343eef4756d8c16edfe6411ca

mongodb-org-repo:
  pkgrepo.managed:
    - baseurl: https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/4.4/x86_64/
    - name: mongodb-org
    - gpgcheck: 1
    - gpgkey: https://www.mongodb.org/static/pgp/server-4.4.asc
    - enabled: True

mongodb-org:
  pkg.installed

mongodb-service:
  service.running:
    - name: mongod
    - enable: true

public:
  firewalld.present:
    - name: public
    - interfaces:
      - ens18
    - ports:
      - 27017/tcp


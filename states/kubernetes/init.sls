# PLEASE PUT DNS ENTRY INTO ROUTER
#
# System specific setup

{% set k8s = salt['pillar.get']('kubernetes') %}

kubernetes-repo:
  file.managed:
    - name: /etc/yum.repos.d/kubernetes.repo
    - source: salt://kubernetes/kubernetes.repo
    - template: jinja
    - context:
      minor_version: {{ k8s.minor_version }}

add_kmods:
  kmod.present:
    - persist: True
    - mods:
      - overlay
      - br_netfilter

net.bridge.bridge-nf-call-iptables:
  sysctl.present:
    - value: 1
  
net.ipv4.ip_forward:
  sysctl.present:
    - value: 1

net.bridge.bridge-nf-call-ip6tables:
  sysctl.present:
    - value: 1

container-runtime-packages:
  pkg.installed:
    - pkgs:
      - cri-o
      - cri-o-runc

container-runtime-service:
  service.running:
    - name: crio
    - enable: true

kubernetes-pkgs:
  pkg.installed:
    - pkgs:
      - kubelet
      - kubeadm
      - kubectl

# Turn swap off
'swapoff -a':
  cmd.run
'touch /etc/systemd/zram-generator.conf':
  cmd.run
#disable swap from fstab
sed -ri '/\sswap\s/s/^#?/#/' /etc/fstab:
  cmd.run

kubelet-service:
  service.running:
    - name: kubelet
    - enable: true

kubelet-api:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - match: state
    - connstate: NEW
    - dport: 10250
    - protocol: tcp
    - save: True
nodeport-services:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - match: state
    - connstate: NEW
    - dport: 30000:32767
    - protocol: tcp
    - save: True

{{ pillar['kubernetes']['join-cmd'] }}:
  cmd.run

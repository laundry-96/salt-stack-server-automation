kubernetes-api-server:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - match: state
    - connstate: NEW
    - dport: 6443
    - protocol: tcp
    - save: True
etcd-server-client:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - match: state
    - connstate: NEW
    - dport: 2379:2380
    - protocol: tcp
    - save: True
kubelet-api:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - match: state
    - connstate: NEW
    - dport: 10250
    - protocol: tcp
    - save: True
kubernetes-scheduler:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - match: state
    - connstate: NEW
    - dport: 10251
    - protocol: tcp
    - save: True
kubernetes-controller-manager:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - match: state
    - connstate: NEW
    - dport: 10252
    - protocol: tcp
    - save: True

'kubadm init {{
'kubeadm init {{ pillar['kubernetes']['kubeadm-init']['cmd-args'] }}':
  cmd.run

/usr/bin/kprep:
  file.managed:
    - source: salt://kubernetes/kprep.sh
    - user: root
    - group: root
    - mode: 755

kprep-root:
  cmd.run:
    - name: kprep
    - runas: root

'kubectl apply -f https://docs.projectcalico.org/manifests/canal.yaml':
  cmd.run

helm-repo:
  pkgrepo.managed:
    - name: deb https://baltocdn.com/helm/stable/debian/ all main
    - file: /etc/apt/sources.list.d/helm-stable-debian.list
    - key_url: https://baltocdn.com/helm/signing.asc

helm:
  pkg.installed

'helm repo add metallb https://metallb.github.io/metallb':
  cmd.run

'helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx':
  cmd.run

'helm repo update':
  cmd.run

/tmp/metallb/values.yaml:
  file.managed:
    - source: salt://kubernetes/metallb-config-values.yaml
    - user: root
    - group: root
    - mode: 755
    - makedirs: true

'helm install metallb metallb/metallb -f /tmp/metallb/values.yaml -n metallb-system --create-namespace':
  cmd.run

'helm install ingress-nginx ingress-nginx/ingress-nginx -n ingress-nginx --create-namespace':
  cmd.run

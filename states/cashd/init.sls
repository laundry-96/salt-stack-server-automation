cashd:
  user.present:
    - fullname: David Cash
    - shell: /bin/zsh

{% for system, args in pillar.get('cashd-account').get('ssh_keys').items() %}
{{ args.key }}:
  ssh_auth.present:
    - user: cashd
    - enc: ed25519
{% endfor %}

cashd-sudoers:
  file.managed:
    - name: /etc/sudoers.d/cashd
    - source: salt://cashd/cashd-sudoers
    - user: root
    - makedirs: true

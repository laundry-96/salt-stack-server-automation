base:

  # Apply OS specific states first
#  'os:Debian':
#    - match: grain
#    - debian

  '*':
    - laundry
    - cashd

  'jumpbox':
    - jumpbox

  'awky-*':
    - awky

  'lcarn-*':
    - lcarn

  'valkyrie-*':
    - valkyrie

  'docker-*':
    - docker

  'kube-*':
    - kubernetes
  'etcd-*':
    - kubernetes

  'kube-master*':
    - kubernetes.master

  'kube-node-*':
    - kubernetes.minion

  'minecraft-*':
    - minecraft
    - minecraft.overviewer

  'seedbox*':
    - nordvpn
    - torrent-server

  'mongodb*':
    - mongodb

  'dns*':
    - dns

lcarn:
  user.present:
    - fullname: Luke Carnaggio
    - shell: /bin/zsh

{% for system, args in pillar.get('lcarn-account').get('ssh_keys').items() %}
{{ args.key }}:
  ssh_auth.present:
    - user: lcarn
    - enc: ssh-ed25519
{% endfor %}

lcarn-sudoers:
  file.managed:
    - name: /etc/sudoers.d/lcarn
    - source: salt://lcarn/lcarn-sudoers
    - user: root
    - makedirs: true

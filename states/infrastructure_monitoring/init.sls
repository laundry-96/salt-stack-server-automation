{{ pillar['prometheus_node_exporter']['pkgname'] }}:
  pkg:
    - installed
  service:
    - running

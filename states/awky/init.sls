awky:
  user.present:
    - fullname: Ashley Wu
    - shell: /bin/zsh

{% for system, args in pillar.get('awky-account').get('ssh_keys').items() %}
{{ args.key }}:
  ssh_auth.present:
    - user: awky
    - enc: ed25519
{% endfor %}

awky-sudoers:
  file.managed:
    - name: /etc/sudoers.d/awky
    - source: salt://awky/awky-sudoers
    - user: root
    - makedirs: true

personal.packages:
  pkg.installed:
    - pkgs:
      - zsh
      - git
      - htop
      {% if grains['os_family']=="RedHat" %}
      - vim-enhanced
      {% elif grains['os_family']=="Debian" %}
      - vim
      - curl
      {% endif %}

laundry:
  user.present:
    - fullname: Austin DeLauney
    - shell: /bin/zsh
    - password: "{{ pillar['laundry-account']['password'] }}"

git-name-config:
  git.config_set:
    - name: user.name
    - value: Austin DeLauney
    - user: laundry
    - global: True

git-email-config:
  git.config_set:
    - name: user.email
    - value: adelauney96@outlook.com
    - user: laundry
    - global: True

laundry_sshkeys:
  ssh_auth.manage:
    - user: laundry
    - enc: ssh-ed25519
    - ssh_keys:
{% for system, args in pillar.get('laundry-account').get('ssh_keys').items() %}
      - {{ args.key }}
{% endfor %}

oh-my-zsh:
  cmd.run:
    - name: sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    - runas: laundry

zshrc:
  file.managed:
    - name: /home/laundry/.zshrc
    - source: salt://laundry/zshrc
    - user: laundry

vimrc:
  file.managed:
    - name: /home/laundry/.vimrc
    - source: salt://laundry/vimrc
    - user: laundry

vundle:
  git.cloned:
    - name: https://github.com/VundleVim/Vundle.vim.git
    - target: /home/laundry/.vim/bundle/Vundle.vim

laundry-sudoers:
  file.managed:
    - name: /etc/sudoers.d/laundry
    - source: salt://laundry/laundry-sudoers
    - user: root
    - makedirs: true

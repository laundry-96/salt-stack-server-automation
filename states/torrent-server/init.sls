transmission.pkgs:
  pkg.installed:
    - pkgs:
      - transmission
      - transmission-daemon

transmission-settings:
  file.managed:
    - name: /var/lib/transmission/.config/transmission-daemon/settings.json
    - source: salt://torrent-server/settings.json
    - owner: transmission
    - makedirs: true

transmission-service:
  service.running:
    - name: transmission-daemon
    - enable: true

public:
  firewalld.present:
    - name: public
    - interfaces:
      - ens18
    - ports:
      - 9091/tcp

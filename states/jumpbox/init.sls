sshd:
  service.running:
    - enable: True
    - watch:
      - file: /etc/ssh/sshd_config
      - file: /etc/motd
      - file: /etc/ssh/banner.txt

/etc/ssh/sshd_config:
  file.managed:
    - source: salt://jumpbox/sshd_config
    - user: root
    - group: root

/etc/motd:
  file.managed:
    - source: salt://jumpbox/motd.txt
    - user: root
    - group: root

/etc/ssh/banner.txt:
  file.managed:
    - source: salt://jumpbox/banner.txt
    - user: root
    - group: root

{% for user, data in pillar.get('jumpbox-users').items() %}
{{ user }}:
  user.present:
    - fullname: {{ data.name }}
    - shell: /bin/zsh

{% for name, jumpkey in data.get('jump-keys').items() %}
{{ jumpkey.get('jump-key') }}:
  ssh_auth.present:
    - user: {{ user }}
    - enc: ssh-ed25519
{% endfor %}
{% endfor %}

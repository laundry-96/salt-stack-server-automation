{% if grains['osfinger']=="CentOS Linux-8" %}
docker-ce-repo:
  pkgrepo.managed:
    - baseurl: https://download.docker.com/linux/centos/7/$basearch/stable
    - name: docker-ce
    - gpgcheck: 1
    - gpgkey: https://download.docker.com/linux/centos/gpg

containerd.io:
  pkg.installed:
    - sources: 
      - containerd.io: https://download.docker.com/linux/centos/7/x86_64/stable/Packages/containerd.io-1.2.6-3.3.el7.x86_64.rpm

docker-ce:
  pkg.installed

public:
  firewalld.present:
    - name: public
    - interfaces:
      - ens18
    - ports:
      - 6443/tcp
      - 2379-2380/tcp
      - 10250/tcp
      - 10251/tcp
      - 10252/tcp
      - 10255/tcp

{% endif %}

add_br_netfilter:
  kmod.present:
    - name: br_netfilter

'echo 1 > /proc/sys/net/bridge/bridge-nf-call-iptables':
  cmd.run

/etc/docker:
  file.directory:
    - user: root
    - group: root

/etc/docker/daemon.json:
  file.managed:
    - source: salt://docker/daemon.json
    - user: root
    - group: root
    - requires:
      - /etc/docker

docker-service:
  service.running:
    - name: docker
    - enable: true
    - watch:
      - file: /etc/docker/daemon.json

/etc/systemd/system/docker.service.d:
  file.directory:
    - user: root
    - group: root

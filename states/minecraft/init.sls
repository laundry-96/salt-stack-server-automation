java:
  pkg.installed:
  {% if grains['os_family']=="RedHat" %}
  - name: java-16-openjdk
  {% elif grains['os_family']=="Debian" %}
  - name: openjdk-16-jdk
  {% endif %}

# This program is needed for the service file, since we use screen to send commands.
screen:
  pkg.installed

minecraft:
  user.present:
    - shell: /bin/zsh
    - home: /opt/minecraft

minecraft-server:
  file.managed:
    - name: /opt/minecraft/server/minecraft_server.jar
    - source: {{ pillar['minecraft-server']['jar']['link'] }}
    - source_hash: {{ pillar['minecraft-server']['jar']['checksum'] }}
    - owner: minecraft
    - mode: 755
    - makedirs: true
    - require:
      - user: minecraft

minecraft-eula:
  file.managed:
    - name: /opt/minecraft/server/eula.txt
    - source: salt://minecraft/eula.txt
    - owner: minecraft
    - makedirs: true
    - require:
      - user: minecraft

minecraft-init:
  file.managed:
    - name: /etc/systemd/system/minecraft.service
    - user: root
    - group: root
    - mode: 644
    - source: salt://minecraft/minecraft.service

minecraft-properties:
  file.managed:
    - name: /opt/minecraft/server/server.properties
    - source: salt://minecraft/server.properties
    - user: minecraft
    - makedirs: true
    - template: jinja

public:
  firewalld.present:
    - name: public
    - interfaces:
      - ens18
    - ports:
      - {{ salt['pillar.get']('minecraft-server:properties:port', '25565') }}/tcp

disabled:
  selinux.mode
 
minecraft-service:
  service.running:
    - name: minecraft
    - enable: True
    - require:
      - file: minecraft-init 
      - file: minecraft-server
      - file: minecraft-eula
      - file: minecraft-properties

java:
  pkg.installed:
  {% if grains['os_family']=="RedHat" %}
  - name: java-11-openjdk
  {% elif grains['os_family']=="Debian" %}
  - name: openjdk-11-jdk
  {% endif %}

# This program is needed for the service file, since we use screen to send commands.
screen:
  pkg.installed

minecraft:
  user.present:
    - shell: /bin/zsh
    - home: /opt/minecraft

forge-installer:
 file.managed:
   - name: /opt/minecraft/server/forge-installer.jar
   - source: {{ pillar['minecraft-server']['forge']['link'] }}
   - source_hash: {{ pillar['minecraft-server']['forge']['checksum'] }}
   - owner: minecraft
   - mode: 755
   - makedirs: true
   - require:
     - user: minecraft

install-forge-server:
  cmd.run:
    - name: java -jar forge-installer.jar --installServer
    - cwd: /opt/minecraft/server
    - runas: minecraft

minecraft-eula:
  file.managed:
    - name: /opt/minecraft/server/eula.txt
    - source: salt://minecraft/eula.txt
    - owner: minecraft
    - makedirs: true
    - require:
      - user: minecraft

minecraft-init:
  file.managed:
    - name: /etc/systemd/system/minecraft-modded.service
    - user: root
    - group: root
    - mode: 644
    - source: salt://minecraft/minecraft-modded.service
    - template: jinja

minecraft-properties:
  file.managed:
    - name: /opt/minecraft/server/server.properties
    - source: salt://minecraft/server.properties
    - user: minecraft
    - makedirs: true
    - template: jinja

public:
  firewalld.present:
    - name: public
    - ports:
      - {{ salt['pillar.get']('minecraft-server:properties:port', '25565') }}/tcp

disabled:
  selinux.mode
 
minecraft-modded-service:
  service.running:
    - name: minecraft-modded
    - enable: True
    - require:
      - file: minecraft-init 
      - file: minecraft-eula
      - file: minecraft-properties

{% if grains['os_family']=="Debian" %}
apt-transport-https:
  pkg.installed

gnupg:
  pkg.installed

repo:
  file.append:
    - name: /etc/apt/sources.list
    - text: deb https://overviewer.org/debian ./

accept-key:
  cmd.run:
    - name: wget -O - https://overviewer.org/debian/overviewer.gpg.asc | sudo apt-key add -
    - runas: root

minecraft-overviewer:
  pkg.installed:
    - refresh: True
    - require:
      - minecraft

{% elif grains['os_family']=="RedHat" %}

repo:
  pkgrepo.managed:
    - name: "Minecraft-Overviewer-28-$basearch"
    - humanname: Minecraft Overviewer
    - baseurl: http://overviewer.org/rpms/28/$basearch
    - enabled: True

Minecraft-Overviewer:
  pkg.installed:
    - require:
      - minecraft

{% endif %}

overviewere-cron:
  cron.present:
    - name: overviewer.py -rendermodes=smooth-lighting ./server/world ./mcmap
    - user: minecraft
    - hour: 12
  require:
    - user: minecraft

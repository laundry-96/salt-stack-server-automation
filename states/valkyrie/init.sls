valkyrie:
  user.present:
    - fullname: Valarie Novak
    - shell: /bin/zsh

{% for system, args in pillar.get('valkyrie-account').get('ssh_keys').items() %}
{{ args.key }}:
  ssh_auth.present:
    - user: valkyrie
    - enc: ssh-ed25519
{% endfor %}

valkyrie-sudoers:
  file.managed:
    - name: /etc/sudoers.d/valkyrie
    - source: salt://valkyrie/valkyrie-sudoers
    - user: root
    - makedirs: true

nordvpn-cli:
  pkg.installed:
    - sources:
      - nordvpn-release: https://repo.nordvpn.com/yum/nordvpn/centos/noarch/Packages/n/nordvpn-release-1.0.0-1.noarch.rpm

nordvpn:
  pkg.installed

nordvpn login -u '{{ pillar['nordvpn']['username'] }}' -p '{{ pillar['nordvpn']['password'] }}':
  cmd.run

nordvpn set autoconnect enabled {{ pillar['nordvpn']['city'] }}:
  cmd.run

nordvpn whitelist add subnet 192.168.0.0/16:
  cmd.run

nordvpn connect:
  cmd.run

set-nordvpn-interface:
  firewalld.present:
    - name: public
    - interfaces:
      - tun0
